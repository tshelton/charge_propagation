submodule (grid_class) grid_class_sub
  implicit none
  
contains

  subroutine gridRead(this, e_filename, w_filename)
    type(Grid), intent(inout) :: this
    character(len=*), intent(in) :: e_filename, w_filename
    
    integer :: i, j, k, ioStatus
    real :: x, y, z, mag ! Coord translate from indices and mags are calcable
    
    open (unit = 20, file = e_filename, status = 'old', action = 'read', &
      iostat = ioStatus)
    if (ioStatus /= 0) STOP "Error opening file "//e_filename
    open(unit=30, file=w_filename, status='old', action='read', iostat=ioStatus)
    if (ioStatus /= 0) STOP "Error opening file "//w_filename
 
    ! Skip headers
    do i = 1, 9; read (20, *); read (30, *); end do
  
    do i = 1, MAX_X
      do j = 1, MAX_Y
        do k = 1, MAX_Z
          ! Read in coord vales, V, skip E-mag, E comps
          read (20, *) x,y,z, this%grid_(V_IND,k,j,i), mag, &
            this%grid_(E_X_IND:E_Z_IND,k,j,i)
           ! Read skip coord vales, V0, skip W-mag, W comps
          read (30, *) x,y,z, this%grid_(V0_IND,k,j,i), mag, &
            this%grid_(W_X_IND:W_Z_IND,k,j,i)
        end do
      end do
    end do 

    close (20); close (30)
  end subroutine gridRead
  
  subroutine gridReadMaybe(this, e_filename, w_filename)
    type(Grid), intent(inout) :: this
    character(len=*), intent(in) :: e_filename, w_filename
    
    integer :: i, j, k, ioStatus
    real :: x, y, z, mag ! Coord translate from indices and mags are calcable
    
    open(unit=20, file=e_filename, status='old', action='read', iostat=ioStatus)
    if (ioStatus /= 0) stop "Error opening file "//e_filename
    open(unit=30, file=w_filename, status='old', action='read', iostat=ioStatus)
    if (ioStatus /= 0) stop "Error opening file "//w_filename
 
    ! Skip header
    do i = 0, 8; read(20, *); read(30, *); end do
  
    read(20, *) x,y,z, this%grid_(:,:,:,V_IND), mag, this%grid_(:,:,:,E_X_IND:E_Z_IND)
    read(30, *) x,y,z, this%grid_(:,:,:,V0_IND), mag, this%grid_(:,:,:,W_X_IND:W_Z_IND)
 
    close(20); close(30)
  end subroutine gridReadMaybe
  
  pure function getVoxelVertices(point) result(vertices)
    ! Return an array of the eight vertices' vals around the given point
    ! This array is indices but real b/c they're the ion's position, so not
    !   located on actual extant grid indices
    real, dimension(3), intent(in) :: point 
    
    ! The vertices are stored in the order natural to the interpolation fnc
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    ! Assuming pt is given in index form
    integer :: x_floor, x_ceil, y_floor, y_ceil, z_floor, z_ceil
    
    ! The six coord vals around the point
    x_floor = floor (point(1)); x_ceil = ceiling (point(1))
    y_floor = floor (point(2)); y_ceil = ceiling (point(2))
    z_floor = floor (point(3)); z_ceil = ceiling (point(3))

    ! The eight vertices surrounding the point
    vertices(:, 1) = [ x_floor, y_floor, z_floor ]
    vertices(:, 2) = [ x_ceil,  y_floor, z_floor ]
    vertices(:, 3) = [ x_floor, y_ceil,  z_floor ]
    vertices(:, 4) = [ x_ceil,  y_ceil,  z_floor ]
    vertices(:, 5) = [ x_floor, y_floor, z_ceil  ]
    vertices(:, 6) = [ x_ceil,  y_floor, z_ceil  ]
    vertices(:, 7) = [ x_floor, y_ceil,  z_ceil  ]
    vertices(:, 8) = [ x_ceil,  y_ceil,  z_ceil  ]

  end function getVoxelVertices

  pure function getNearestVertex(point) result(vertex)
    real, dimension(3), intent(in) :: point
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    
    integer, dimension(3) :: vertex
    
    real :: distance, nearestDistance
    integer :: i

    vertices = getVoxelVertices (point)
    
    nearestDistance = 1.0e12
    do i = 1, NUM_VOXEL_VERTICES
      distance = norm2 (vertices(:, i) - point)
      if (distance < nearestDistance) then
        nearestDistance = distance
        vertex = vertices(:, i)
      end if
    end do
  end function getNearestVertex

  real pure function getValAtVertex(this, point, val_ind) result(val)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point
    
    integer, intent(in) :: val_ind
    
    val = this%grid_(val_ind, point(3), point(2), point(1))
  end function getValAtVertex
  
  pure function getElectricField(this, point) result(electric_field)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point

    real, dimension(3) :: electric_field
    
    electric_field = this%grid_(E_X_IND:E_Z_IND, point(3), point(2), point(1))
  end function getElectricField

  pure function getWeightingField(this, point) result(weighting_field)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point

    real, dimension(3) :: weighting_field
    
    weighting_field = this%grid_(W_X_IND:W_Z_IND, point(3), point(2), point(1))
  end function getWeightingField

  pure function getVoxelVerticesValues(this, point) result(vals)
    real, dimension(3), intent(in) :: point
    type(Grid), intent(in) :: this

    real, dimension(NUM_GRID_VALS, NUM_VOXEL_VERTICES) :: vals
    
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    integer :: i
    
    vertices = getVoxelVertices (point)
    ! Get the values on the grid at the vertices
    do i = 1, NUM_VOXEL_VERTICES
      vals(:, i) = this%grid_(:, vertices(3, i), vertices(2, i), &
        vertices(1, i))
    end do
  end function getVoxelVerticesValues
  
  real pure function getGridValue(this, vertex, which_val) result(val)
    ! Return a specified value at a specified vertex in this grid
    ! Mostly b/c it is cleaner access
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: vertex
    integer, intent(in) :: which_val

    val = this%grid_(which_val, vertex(3), vertex(2), vertex(1))
  end function getGridValue

  real pure function trilinearInterpolateDatum(this, vertices, point, grid_datum) &
      result(interpolated_grid_datum)
    ! Contains some machinary to interpolate some value in the grid given an 
    !   adjusted point and the vertices surrounding 
    type(Grid), intent(in) :: this
    integer, dimension(3, NUM_VOXEL_VERTICES), intent(in) :: vertices
    integer, intent(in) :: grid_datum
    real, dimension(3), intent(in) :: point ! Assume pre-adjusted
    
    real, dimension(NUM_VOXEL_VERTICES) :: grid_data
    integer :: i
    
    do i = 1, NUM_VOXEL_VERTICES
      grid_data(i) = this%grid_(grid_datum, vertices(3, i), vertices(2, i), &
                                     vertices(1, i))
    end do

    interpolated_grid_datum = trilinearInterpolate (grid_data, point)
  end function trilinearInterpolateDatum
  
  pure function nearestNeighborInterpolatedElectricField(this, point) & 
      result(electric_field)
    ! Find the electric field at some given point using nearest neighbor 
    !   interpolation
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point

    real, dimension(3) :: electric_field
    
    integer, dimension(3) :: vertex

    vertex = getNearestVertex (point)
    
    electric_field = [getGridValue (this, vertex, E_X_IND), &
                      getGridValue (this, vertex, E_Y_IND), &
                      getGridValue (this, vertex, E_Z_IND)]
  end function nearestNeighborInterpolatedElectricField

  pure function trilinearInterpolatedElectricField(this, point) &
      result(electric_field)
    ! Find the electric field at gome given point using triliear interpolation
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point

    real, dimension(3) :: electric_field
    
    real, dimension(3) :: adjusted_point
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    integer, dimension(3) :: vertex
    
    vertices = getVoxelVertices (point)
    vertex = vertices(:, 1) ! The floored indices
    adjusted_point(1:2) = (point(1:2) - vertex(1:2))!/this%xtal_grid_
    adjusted_point(3) = (point(3) - vertex(3))!/this%ztal_grid_

    electric_field = &
      [trilinearInterpolateDatum (this, vertices, adjusted_point, E_X_IND), &
       trilinearInterpolateDatum (this, vertices, adjusted_point, E_Y_IND), &
       trilinearInterpolateDatum (this, vertices, adjusted_point, E_Z_IND)]
  end function trilinearInterpolatedElectricField

  pure function nearestNeighborInterpolatedWeightingField(this, point) & 
      result(weighting_field)
    ! Find the weighting field at some given point using nearest neighbor 
    !   interpolation
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point
    
    real, dimension(3) :: weighting_field
    
    integer, dimension(3) :: vertex

    vertex = getNearestVertex (point)

    weighting_field = [getGridValue (this, vertex, W_X_IND), &
                       getGridValue (this, vertex, W_Y_IND), &
                       getGridValue (this, vertex, W_Z_IND)]
  end function nearestNeighborInterpolatedWeightingField

  pure function trilinearInterpolatedWeightingField(this, point) &
      result(weighting_field)
    ! Find the weighting field at gome given point using triliear interpolation
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point

    real, dimension(3) :: weighting_field
    
    real, dimension(3) :: adjusted_point
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    integer, dimension(3) :: vertex
    
    vertices = getVoxelVertices (point)
    vertex = vertices(:, 1) ! The floored indices
    adjusted_point(1:2) = (point(1:2) - vertex(1:2))!/this%xtal_grid_
    adjusted_point(3) = (point(3) - vertex(3))!/this%ztal_grid_

    weighting_field = &
      [trilinearInterpolateDatum (this, vertices, adjusted_point, W_X_IND), &
       trilinearInterpolateDatum (this, vertices, adjusted_point, W_Y_IND), &
       trilinearInterpolateDatum (this, vertices, adjusted_point, W_Z_IND)]
  end function trilinearInterpolatedWeightingField
  
  pure function geometricToIndices(this, point_geo) result(point_ind)
    ! Takes a point with geometric values and converts to grid indices
    real, dimension(3), intent(in) :: point_geo
    type(Grid), intent(in) :: this
    
    real, dimension(3) :: point_ind
    
    point_ind(1:2) = (point_geo(1:2) + xtal_width*0.5) / xtal_grid
    point_ind(3) = point_geo(3)/ztal_grid
    
  end function geometricToIndices
  
  pure function geometricToTrueIndex(this, point_geo) result(point_ind)
    ! Takes a point with geometric values and converts to grid indices
    real, dimension(3), intent(in) :: point_geo
    type(Grid), intent(in) :: this
    
    integer, dimension(3) :: point_ind
    
    point_ind(1:2) = int ((point_geo(1:2) + xtal_width*0.5) / &
                            xtal_grid)
    point_ind(3) = int(point_geo(3)/ztal_grid)
    
  end function geometricToTrueIndex
  
  pure function indicesToGeometric(this, point_ind) result(point_geo)
    ! Takes a point of grid indices values and converts to the geometric
    integer, dimension(3), intent(in) :: point_ind
    type(Grid), intent(in) :: this
    
    real, dimension(3) :: point_geo
    
    point_geo(1:2) = xtal_grid * point_ind(1:2) - xtal_width*0.5
    point_geo(3) = ztal_grid * point_ind(3)
  end function indicesToGeometric

end submodule grid_class_sub
