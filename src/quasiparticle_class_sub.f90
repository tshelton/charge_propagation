submodule (quasiparticle_class) quasiparticle_class_sub
  implicit none

contains
  integer pure function getCharge(this) result(ion_charge)
    type(quasiparticle), intent(in) :: this

    ion_charge = this%q_
  end function getCharge
  
  pure function getCoordinates(this) result(coordinates)
    type(Quasiparticle), intent(in) :: this
    
    real, dimension(3) :: coordinates
    
    coordinates = this%coordinates_
  end function getCoordinates
  
  pure subroutine setCoordinates(this, new_coordinates)
    type(Quasiparticle), intent(inout) :: this
    
    real, dimension(3), intent(in) :: new_coordinates
    
    this%coordinates_ = new_coordinates
  end subroutine setCoordinates

  pure subroutine init(this, is_electron)
    type(Quasiparticle), intent(inout) :: this
    logical, intent(in) :: is_electron
    
    if (is_electron) then
      this%q_  = q_e
      this%mu_ = mu_e
    else 
      this%q_  = q_h
      this%mu_ = mu_h
    end if
  end subroutine init

  pure function velocity(this, electric_field)
    ! Get velocity of ion, trivially done, but could increase complexity scope
    type(Quasiparticle), intent(in) :: this
    real, dimension(3), intent(in) :: electric_field
    
    ! We just assume bias voltage low enough that vel proportional to e field
    ! i.e. v = mu*E
    velocity = this%mu_*electric_field
  end function velocity

  pure subroutine move(this, velocity, time_step)
    type(Quasiparticle), intent(inout) :: this
    real, dimension(3), intent(in) :: velocity
    real, intent(in) :: time_step

    ! r = r_0 + v*t
    this%coordinates_ = this%coordinates_ + time_step*velocity
  end subroutine

  real pure function inducedCurrent(this, velocity, weighting_field)
    type(Quasiparticle), intent(in) :: this
    real, dimension(3), intent(in) :: velocity, weighting_field

    ! i=q<v,E_0>
    inducedCurrent = this%q_*sum(velocity*weighting_field)
  end function inducedCurrent
end submodule quasiparticle_class_sub

