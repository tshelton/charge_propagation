submodule (incidentParticle_class) incidentParticle_class_sub
  use functions_mod
  implicit none

contains
  type(incidentParticle) function new_particle(init_loc, energy)
    real, dimension(3), intent(in) :: init_loc
    real, intent(in) :: energy

    new_particle%incident_energy_ = energy
    new_particle%entry_location_ = init_loc
    ! x, y locs don't currently change over the deposition
    new_particle%locations_and_energy_deposited_(:,1) = init_loc(1)
    new_particle%locations_and_energy_deposited_(:,2) = init_loc(2)
    new_particle%locations_and_energy_deposited_(:,3:4) = &
      depositionDistribution (new_particle)
  end function new_particle
  
  type(incidentParticle) function new_particle_from_file(filename)
    character(len=*), intent(in) :: filename
    
    
  end function new_particle_from_file
  
  subroutine initParticle(this, init_loc, energy)
    type(incidentParticle), intent(inout) :: this
    real, dimension(3), intent(in) :: init_loc
    real, intent(in) :: energy

    this%incident_energy_ = energy
    this%entry_location_ = init_loc
    ! x,y locs don't currently change over the deposition
    this%locations_and_energy_deposited_(:,1) = init_loc(1)
    this%locations_and_energy_deposited_(:,2) = init_loc(2)
    ! Just fill z_locs and and energy deposited for now
    this%locations_and_energy_deposited_(:,3:4) = depositionDistribution (this)
  end subroutine

  real pure function getIncidentEnergy(this)
    type(incidentParticle), intent(in) :: this

    getIncidentEnergy = this%incident_energy_
  end function getIncidentEnergy

  pure function getEntryLocation(this)
    type(incidentParticle), intent(in) :: this

    getEntryLocation = this%entry_location_
  end function getEntryLocation
  
  pure function getDepositionLocation(this, dep_ind) result(dep_coords)
    type(incidentParticle), intent(in) :: this
    integer, intent(in) :: dep_ind

    real, dimension(3) :: dep_coords

    dep_coords = this%locations_and_energy_deposited_(dep_ind,1:3)
  end function getDepositionLocation

  real pure function getDepositionEnergy(this, dep_ind) 
    type(incidentParticle), intent(in) :: this
    integer, intent(in) :: dep_ind
    
    getDepositionEnergy = this%locations_and_energy_deposited_(dep_ind, 4)
  end function getDepositionEnergy

  pure function getAllDepositionLocations(this) result(locations)
    type(incidentParticle), intent(in) :: this

    real, dimension(NUM_DEPOSITION_BINS,3) :: locations
    
    locations(:,:) = this%locations_and_energy_deposited_(:,1:3)
  end function getAllDepositionLocations

  pure function getAllDepositionEnergies(this) result(energies)
    type(incidentParticle), intent(in) :: this

    real, dimension(NUM_DEPOSITION_BINS) :: energies

    energies = this%locations_and_energy_deposited_(:,4)
  end function getAllDepositionEnergies

  function depositionDistribution(this) result(locs_and_e_deps)
    ! Returns the locations and energy deposition distribution for the particle
    ! Outline of function:
    !   Get the params for this energy
    !   Calc deposition distro for energy using params
    !   Find the (rough) end of the distro
    !   Recalc dep distro without excess but now binned more discretly
    !   Return the bin centers and how much energy deposited there
    
    type(incidentParticle), intent(in) :: this
    real, dimension(NUM_DEPOSITION_BINS, 2) :: locs_and_e_deps
    
    ! Some local parameters to explain what's happening
    real, parameter :: ONE_EV = 0.001 ! For dep truncation
    integer, parameter :: DETCT_START = 1, & ! If want to change
                          DETCT_STEP  = 1, & ! Note sure why this would, but
                          DETCT_DEPTH = 2000 ! ~[um]
    
    real :: gaus_amp, mean, sigma, hypermet_amp, tau
    ! Temp arrays used in calculating the dep distro
    real, dimension(DETCT_DEPTH) :: z_depth, energy_counts
    real :: bin_width
    integer :: i, last_non_zero_energy_dep

    ! Intially generate the dep distro over the whole detector at 1um steps
    z_depth = [(i, i = DETCT_START, DETCT_DEPTH, DETCT_STEP)]

    ! Calculate params from fits in files given our energy
    gaus_amp     = distributionParameter (this, GAUS_AMP_PARAM)
    mean         = distributionParameter (this, MEAN_PARAM)
    sigma        = distributionParameter (this, SIGMA_PARAM)
    hypermet_amp = distributionParameter (this, HYP_AMP_PARAM)
    tau          = distributionParameter (this, TAU_PARAM)
   
    ! The energy deposition distribution
    energy_counts = gaustail (z_depth, gaus_amp, mean, sigma, hypermet_amp, &
      tau)

    ! Find where the deposition ends, roughly in that it just handwaves 
    !   numerical precision and such
    last_non_zero_energy_dep = -1
    do i = 1, size (energy_counts)
      if (energy_counts(i) .lt. ONE_EV) then
        last_non_zero_energy_dep = i
        exit
      end if
    end do
    ! If dep spans the whole detector, e.g. near 1 MeV
    if (last_non_zero_energy_dep == -1) then
      last_non_zero_energy_dep = size (energy_counts)
    end if
   
    ! The max_depth being the last bin only works for the current step size,
    !   note for if that is ever changed
    bin_width = (last_non_zero_energy_dep*1.0)/NUM_DEPOSITION_BINS
    
    ! Calc bin centers
    do i = 1, NUM_DEPOSITION_BINS
      locs_and_e_deps(i, 1) = ((i-1) + 0.5)*bin_width
    end do

    locs_and_e_deps(:,2) = gaustail (locs_and_e_deps(:,1), gaus_amp, mean, &
      sigma, hypermet_amp, tau)

    ! Normalize the area to unity b/c the fits are not perfect, then scale by 
    !   the incident energy, i.e. make the area under the curve = the energy
    locs_and_e_deps(:,2) = locs_and_e_deps(:,2) * this%incident_energy_ / &
      integratetrapz (locs_and_e_deps(:,1), locs_and_e_deps(:,2))
  end function depositionDistribution

  real function distributionParameter(this, which_param, op_event_type)
    ! Calculates some (which_param) parameter for some event type (optional and 
    !   defaults to all) by reading in from the appropriate file.
    type(incidentParticle), intent(in) :: this
    character(len=*), intent(in) :: which_param
    integer, optional, intent(in) :: op_event_type

    real, dimension(NUM_EVENT_TYPES) :: p0, p1, p2
    integer :: ioStatus, event_type
    real :: energy

    ! Which event type are we calculating for?
    if (present (op_event_type)) then
      event_type = op_event_type
    else
      event_type = EVENT_TYPE_ALL ! Default
    end if
    
    energy = this%incident_energy_

    ! Read and calc parameter
    ! Different degree polynomial functions
    if (which_param == GAUS_AMP_PARAM .or. which_param == HYP_AMP_PARAM) then
      open(unit=10, file=DIST_PARAMS_LOC//which_param, status='old', &
        action='read', iostat=ioStatus)
      if (ioStatus /= 0) stop "Error opening file "//DIST_PARAMS_LOC//&
                                  which_param
      read(10,*) p0, p1
      close(10)
      ! param = p0 + p1*e
      distributionParameter = p0(event_type) + p1(event_type)*energy
    else
      open(unit=10, file=DIST_PARAMS_LOC//which_param, status='old', &
        action='read', iostat=ioStatus)
      if (ioStatus /= 0) stop "Error opening file "//DIST_PARAMS_LOC//&
                                which_param
      read(10,*) p0, p1, p2
      close(10)
      ! param = p0 + p1*e + p2*e^2
      distributionParameter = (p2(event_type)*energy + p1(event_type))* &
        energy + p0(event_type)
    end if
  end function distributionParameter

  real function integrateTrapz(x, y)
    real, intent(in) :: x(:)
    real, intent(in) :: y(size (x))
   
    integrateTrapz = sum ((y(2:) + y(:size(x)-1))*(x(2:) - x(:size(x)-1)))/2
  end function integrateTrapz
end submodule incidentParticle_class_sub
