charge_prop
=============

Contact
-------------
Contact Tom Shelton (ts21@illinois.edu) for questions etc. Or Leah Broussard (broussardlj@ornl.gov).

Requirements
------------

This code is written in Fortran (just 08 standard).  It was tested using GNU Fortran 7.4.0.  The software is intended for use in Linux-based distributions.

Directions
----------

To build the code, create some build directory and run 
```
$ cmake ..
```

Then run, for actual exe,
```
$ make clean
$ make
```

in the build directory.  The executable is run from this directory.  Modules files are in the inc/ directory and submodules/main source code is in the src/ directory.

Acknowledgements
----------------

charge_prop is based on work supported by the U.S. Department of Energy, Office of Science, Office of Nuclear Physics and Office Workforce Development for Teachers and Scientists

Copyright and license
---------------------
Copyright 2019 UT-Battelle, LLC. All rights reserved.

charge_propagation Documentation
===========================
This software takes regular grids of electric/weighting fields and some distribution of energy deposition to determine induced current. Chiefly, it tracks the movement of electron-hole pairs and the induced current from their motion using Shockely-Ramo theorem.

This documentation describes the classes contained in this distribution.

Main program
------------

The main program source code is in src/charge_prop.f90.  Run the exe with the addition of what energy(s) to determine current for:

```
$ ./charge_prop [start energy] [stop energy] [energy step]

$ ./charge_prop [energy] ! For only one distro
```

Modules
------------

#### incidentParticle_class

Handles determining the location/number of ion pairs thoughout the detector.

#### quasiparticle_class

Handles the kinematics and current of the electrons or holes.

#### grid_class

Handles the imported regular E/E_0 fields

#### interpolate_mod

Contains some simple linear interpolations. Used by the grid.

#### functions_mod

Defines function (e.g. hypermet, gaustail) for the distros.

#### filters_mod

Contains an rc - cr^2 filter to simulate electronics for creating waveforms

