module filters_mod
  implicit none
        
contains
  pure function rc_cr2(data_in) result(wf)
    real, dimension(:), intent(in) :: data_in

    real, dimension(size (data_in)) :: wf
    
    real, parameter ::            &
      T       = 0.1,              &
      CRtau   = 150.,             &
      CRalpha = exp(-1.*T/CRtau), &
      RCtau   = 6.,               &
      RCalpha = exp(-1.*t/RCtau)

    ! Zero indexing makes even checking below cleaner
    real, dimension(0:1) :: cr, rc1, rc2
    integer :: i

    !From stacking filters from http://www.analog.com/media/en/technical-documentation/dsp-book/dsp_book_Ch19.pdf
    wf = 0.0; cr = 0.0; rc1 = 0.0; rc2 = 0.0

    do i = 4, size (data_in)
      cr(mod (i, 2)) = ((1. + CRalpha)/2.)*(data_in(i) - data_in(i-1)) + &
        CRalpha*cr(mod (i+1, 2))
      
      rc1(mod (i, 2)) = (1. - RCalpha)*cr(mod (i, 2)) + &
        RCalpha*rc1(mod (i+1, 2))
      
      rc2(mod (i, 2)) = (1. - RCalpha)*rc1(mod (i, 2)) + &
        RCalpha*rc2(mod (i+1, 2))
      
      wf(i) = rc2(mod (i, 2))
    end do
  end function rc_cr2
end module filters_mod
