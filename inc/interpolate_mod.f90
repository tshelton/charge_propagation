module interpolate_mod
  ! Contains a few self-contained interpolations methods
  implicit none

  interface

  real pure module function linearInterpolate(vals, x) result(interp_val)
    real, dimension(0:1), intent(in) :: vals
    real, intent(in) :: x
  end function linearInterpolate

  real pure module function bilinearInterpolate(vals, coord) result(interp_val)
    real, dimension(0:3), intent(in) :: vals  
    real, dimension(0:1), intent(in) :: coord
  end function bilinearInterpolate

  real pure module function trilinearInterpolate(vals, coord) result(interp_val)
    real, dimension(0:7), intent(in) :: vals
    real, dimension(0:2), intent(in) :: coord
  end function trilinearInterpolate

  end interface
end module interpolate_mod
