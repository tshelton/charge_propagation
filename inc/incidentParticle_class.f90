module incidentParticle_class
  use functions_mod
  implicit none

  !---------------------------------------------------------------------------
  !   PARAMETERS FOR CLASS
  !---------------------------------------------------------------------------
  !real, parameter :: ENERGY_FOR_PAIR = 3.62 !eV, T=300k
  real, parameter :: &
    ENERGY_FOR_PAIR = 3.76 !eV, T=77K
  integer, parameter :: &
    NUM_DEPOSITION_BINS = 80
  integer, parameter ::  &
    EVENT_TYPE_ALL  = 1, &
    EVENT_TYPE_A    = 2, &
    EVENT_TYPE_B    = 3, &
    EVENT_TYPE_C    = 4, &
    NUM_EVENT_TYPES = 4
  ! Includes file extension for opening the files
  character(len=*), parameter ::             &
    GAUS_AMP_PARAM  = "gaus_amp_params.csv", &
    MEAN_PARAM      = "mean_params.csv",     &
    SIGMA_PARAM     = "sigma_params.csv",    &
    HYP_AMP_PARAM   = "hyp_amp_params.csv",  &
    TAU_PARAM       = "tau_params.csv",      &
    DIST_PARAMS_LOC = "../distribution_params/"
  !---------------------------------------------------------------------------
  
  type incidentParticle
    real :: incident_energy_
    real, dimension(3) :: entry_location_
    real, dimension(NUM_DEPOSITION_BINS, 4) :: locations_and_energy_deposited_
  end type incidentParticle

  interface incidentParticle
    module procedure :: new_particle, new_particle_from_file
  end interface incidentParticle

  interface
  
  type(incidentParticle) module function new_particle(init_loc, energy)
    real, dimension(3), intent(in) :: init_loc
    real, intent(in) :: energy
  end function new_particle

  type(incidentParticle) module function new_particle_from_file(filename)
    character(len=*), intent(in) :: filename
  end function new_particle_from_file

  module subroutine initParticle(this, init_loc, energy)
    type(incidentParticle), intent(inout) :: this
    real, dimension(3), intent(in) :: init_loc
    real, intent(in) :: energy
  end subroutine

  real pure module function getIncidentEnergy(this)
    type(incidentParticle), intent(in) :: this
  end function getIncidentEnergy

  pure module function getEntryLocation(this)
    type(incidentParticle), intent(in) :: this
    real, dimension(3) :: getEntryLocation
  end function getEntryLocation
  
  pure module function getDepositionLocation(this, dep_ind) result(dep_coords)
    type(incidentParticle), intent(in) :: this
    integer, intent(in) :: dep_ind

    real, dimension(3) :: dep_coords
  end function getDepositionLocation

  real pure module function getDepositionEnergy(this, dep_ind) 
    type(incidentParticle), intent(in) :: this
    integer, intent(in) :: dep_ind
  end function getDepositionEnergy

  pure module function getAllDepositionLocations(this) result(locations)
    type(incidentParticle), intent(in) :: this

    real, dimension(NUM_DEPOSITION_BINS,3) :: locations
  end function getAllDepositionLocations

  pure module function getAllDepositionEnergies(this) result(energies)
    type(incidentParticle), intent(in) :: this

    real, dimension(NUM_DEPOSITION_BINS) :: energies
  end function getAllDepositionEnergies

  module function depositionDistribution(this) result(locs_and_e_deps)
    type(incidentParticle), intent(in) :: this
    real, dimension(NUM_DEPOSITION_BINS, 2) :: locs_and_e_deps
    
  end function depositionDistribution

  real module function distributionParameter(this, which_param, op_event_type)
    type(incidentParticle), intent(in) :: this
    character(len=*), intent(in) :: which_param
    integer, optional, intent(in) :: op_event_type
  end function distributionParameter

  real module function integrateTrapz(x, y)
    real, intent(in) :: x(:)
    real, intent(in) :: y(size (x))
  end function integrateTrapz
  
  end interface
  
end module incidentParticle_class
